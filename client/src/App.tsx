import React from 'react'
import Header from './components/Header'
import Section, { ISection } from './components/Section'
import details from './content/details.json'

interface IDetails {
  name: string
  biography: string
  sections: ISection[]
}

const App: React.FC = () => {
  const sectionsRefs = (details as IDetails).sections.reduce((acc, value) => {
    acc[value.name] = React.createRef()
    return acc
  }, {} as { [name: string]: React.RefObject<HTMLDivElement> } )

  return (
      <div className='flex flex-col h-screen text-gray-800 font-cairo'>
        <Header name={details.name} refs={sectionsRefs} />
        <div className='flex flex-grow justify-center overflow-y-auto bg-gray-200'>
          <div className='sm:w-2/3 max-w-5xl'>
            <div className='flex flex-col flex-grow pb-1 md:pb-5'>
              {(details.sections as ISection[]).map((section, index) => (
                <Section ref={sectionsRefs[section.name]} key={index} {...section} />
              ))}
            </div>
          </div>
        </div>
      </div>
  )
}

export default App;
