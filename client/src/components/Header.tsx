import React from 'react'

export interface IHeader {
  name: string
  refs: { [name: string]: React.RefObject<HTMLDivElement> }
}

const Header: React.FC<IHeader> = ({ name, refs }) => {
  const onClick: (name: string) => void = name => {
    const ref = refs[name].current

    if (!ref) {
      return
    }

    ref.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  }

  return (
    <div className='flex justify-center w-full h-12 bg-white border-b border-gray-300'>
      <div className='flex justify-between items-center w-full pl-4 pr-3 sm:px-0 sm:w-2/3 max-w-5xl'>
        <div className='text-lg font-bold tracking-wide'>
          {name}
        </div>
        <div className='hidden sm:flex'>
          {Object.keys(refs).map((name, index) => (
            <div key={index} className='flex items-center capitalize px-2 font-semibold h-12 hover:bg-green-200' onClick={() => onClick(name)}>{name}</div>
          ))}
        </div>
      </div>
    </div>
  )
}
export default Header