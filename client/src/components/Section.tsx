import React from 'react'
import Card, { ICard } from './Card'

export interface ISection {
  name: string
  cards: ICard[]
}

const Section = React.forwardRef<HTMLDivElement, ISection>(({ name, cards }, ref) => (
  <div ref={ref} className='pt-1 md:pt-4'>
    <div className='bg-white sm:rounded-lg px-4 sm:px-6 pt-3 pb-2 sm:border sm:border-gray-300'>
      <span className='text-xl sm:text-2xl font-bold tracking-wide px-2 capitalize border-b-4 border-green-400'>{name}</span>
      {cards.map((card, index) => (
        <Card key={index} {...card} />
      ))}
    </div>
  </div>
))

export default Section