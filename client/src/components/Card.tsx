import React from 'react'

export interface ICard {
  name: string
  description: string
  image: string
  position?: string
  city?: string
  state?: string
  startYear?: number
  stopYear?: number | string
}

const Card: React.FC<ICard> = ({ name, description, startYear, stopYear = 'present', image }) => (
  <div className='flex flex-col items-center sm:flex-row sm:items-baseline bg-white py-3 sm:pt-4 sm:pb-6 px-2 sm:px-4 border-b last:border-b-0 border-gray-300'>
    <div className='flex flex-col w-32 items-center sm:items-baseline'>
      <div className='font-semibold text-center sm:text-left text-md'>
        {name}
      </div>
      {startYear && <div className='text-gray-600 font-semibold text-sm'>{startYear === stopYear ? startYear : `${startYear} - ${stopYear}`}</div>}
      <div className={`flex-grow-0 mb-0 sm:mb-4 w-32 ${image ? 'h-32' : ''}`}>
        {image && <img src={require(`../assets/images/${image}.jpg`)} alt='' className='rounded-lg object-cover mt-2 sm:mt-4' />}
      </div>
    </div>
    <div className='mt-4 sm:mt-0 sm:ml-6'>
      {description}
    </div>
  </div>
)

export default Card