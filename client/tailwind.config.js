module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [
    './src/**/*.tsx',
    './**/src/**/*.tsx'
  ],
  theme: {
    extend: {
      fontFamily: {
        cairo: ['Cairo', 'sans-serif']
      }
    },
  },
  variants: {
    borderWidth: ['responsive', 'last']
  },
  plugins: [],
}
