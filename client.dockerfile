FROM node:alpine as build

WORKDIR /usr/src/app

COPY ./client .

ENV NODE_ENV production

RUN yarn install --silent && yarn build

FROM nginx:alpine

COPY --from=build /usr/src/app/build /usr/share/nginx/html

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]